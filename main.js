import './css/style.css'

let percent
/************************ VARIABLES ************************/
// INPUTS
let billInput = document.querySelector("#bill__input input")
let peopleInput = document.querySelector("#people__input")
let warning = document.querySelector(".warning")

// BUTTONS
let percentages = document.querySelectorAll(".percent__values p")
let reset = document.querySelector(".reset")
let calc = document.querySelector(".calc")

//VALUES
let tipAmount = document.querySelector(".calculator__amount > p")
let total = document.querySelector(".calculator__person > p")


/************************ EVENTS ************************/
window.addEventListener("DOMContentLoaded",() => {
    billInput.value = "142.55"
    peopleInput.value = "5"
})

percentages.forEach(p => {
    p.addEventListener('click',() => {
        percentages.forEach(l => {
            l.classList.remove("chosen")
        })
        p.classList.add("chosen")

        percent = parseFloat(p.textContent.replace("%",""))
        console.log(percent);
        if(p.getAttribute("contenteditable") == "true" && isNaN(p.innerText)) {
            p.innerText = ""
        }
    })
})

peopleInput.addEventListener("keyup",() => {
    warning.classList.remove("appear")
    peopleInput.classList.remove("invalid")
    if(peopleInput.value == 0) {
        warning.classList.add("appear")
        peopleInput.classList.add("invalid")
    }
})

calc.addEventListener("click",() => {
    if(billInput.value == "" || peopleInput.value == "" || peopleInput.value == "0" || percent == undefined) return 
    let bill = parseFloat(billInput.value)
    let persons = parseFloat(peopleInput.value)
    let result = ((bill * percent) / 100) / persons
    console.log(result.toFixed(2));

    tipAmount.textContent = "$" + result.toFixed(2)
    total.textContent = "$" + (bill / persons + result).toFixed(2)
})

reset.addEventListener("click",() => {
    billInput.value = ""
    peopleInput.value = "1"

    tipAmount.textContent = "$0.00"
    total.textContent = "$0.00"
})